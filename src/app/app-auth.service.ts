import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable()
export class AppAuthService {
  url: any;

  constructor(private http: HttpClient, private router: Router) {
    this.url = 'http://192.168.8.108:43210';
  }

  login(login: string, password: string) {
    return this.http.post( this.url + '/oauth/token?grant_type=password&username=' + login + '&password=' + password, null
      , {headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': 'Basic dHJ1c3RlZC1jbGllbnQ6c2VjcmV0'})})
      .subscribe(response => {
          const token = response['access_token'];
          localStorage.setItem('token', token);
          console.log(localStorage.getItem('username'));
          this.router.navigate(['/dashboard']);
        },
        error => console.log(error)
      );
  }

  isLoggedIn() {
    return !!localStorage.getItem('token');
  }

}
